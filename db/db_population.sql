USE COMPRATEC_DB

INSERT INTO users VALUES 
	(1, 'fastorga', '1234'),
	(2, 'dbadilla', 'hola'),
	(3, 'jrojas', '4321')

INSERT INTO orders VALUES 
	(1, 'Fabian Astorga', 'Limpieza'),
	(2, 'David Badilla', 'Electronicos'),
	(3, 'Jaime Rojas', 'Comestible'),
	(4, 'Fabian Astorga', 'Electrodomesticos'),
	(5, 'Raul Madrigal', 'Electrodomesticos'),
	(6, 'Fabian Astorga', 'Comestible'),
	(7, 'David Badilla', 'Limpieza')
	