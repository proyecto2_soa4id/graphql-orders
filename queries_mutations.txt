---------------------------QUERIES----------------------------------------

OBTIENE TODAS LAS ORDENES
{
	"query" : "{ allOrders { owner type }}"
}

OBTIENE ORDEN(ES) SEGUN EL PROPIETARIO 
{
	"query" : "{ getOrderByOwner(owner:\"Fabian Astorga\") { type } }"
}

OBTIENE ORDEN(ES) SEGUN TIPO
{
	"query" : "{ getOrderByType(type:\"Limpieza\") { owner } }"
}

---------------------------MUTATIONS--------------------------------------

CREA UNA ORDEN
{
	"query" : "mutation { createOrder(id:8, owner:\"Jaime Rojas\", type:\"Electronicos\") { id owner type } }"
}

ACTUALIZA EL TIPO DE UNA O VARIAS ORDENES
{
	"query" : "mutation { updateOrderByType(type:\"Otros\", newType:\"Cortinas y sabanas\")}"
}

ELIMINA UNA ORDEN SEGUN SU ID
{
	"query" : "mutation { deleteOrderById(id:8)}"
}