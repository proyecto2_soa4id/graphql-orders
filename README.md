# GraphQL Server with NodeJS and Sequelize in order to connect this with PostgreSQL.

To compile the project, type the following command: 

`
npm install
`

To run Node Server, insert the following command inside `src` folder:

`
npm start
`