export default `
  type Order {
    id: Int!
    owner: String!
    type: String!
  }

  type Query {
    allOrders: [Order!]!
    getOrderByOwner(owner: String!): [Order!]!
    getOrderByType(type: String!): [Order!]!
  }
  
  type Mutation {
    createOrder(id: Int!, owner: String!, type: String!): Order
    updateOrderByType(type: String!, newType: String!): [Int!]!
    deleteOrderById(id: Int!): Int!
  }
`;