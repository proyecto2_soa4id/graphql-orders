export default {
  Query: {
    allOrders: (parent, args, { models }) => models.Order.findAll(),
    getOrderByOwner: (parent, { owner }, { models }) => models.Order.findAll({ where: { owner } }),
    getOrderByType: (parent, { type }, { models }) => models.Order.findAll({ where: { type } })
  },

  Mutation: {
    createOrder: (parent, args, { models }) => models.Order.create(args),
    updateOrderByType: (parent, { type, newType }, { models }) =>
      models.Order.update({ type: newType }, { where: { type } }),
    deleteOrderById: (parent, {id}, { models }) =>
      models.Order.destroy({ where: {id} })
  }
};