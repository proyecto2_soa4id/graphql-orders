import express from 'express';
import bodyParser from 'body-parser';
import { graphqlExpress } from 'graphql-server-express';
import { makeExecutableSchema } from 'graphql-tools';

import typeDefs from './graphql/schema';
import resolvers from './graphql/resolvers';
import models from './models';

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

const default_port = 5001;
const app = express();

app.use(
  '/',
  bodyParser.json(),
  graphqlExpress({ schema, graphiql : true, context: { models } }),
);

models.sequelize.sync().then(() => app.listen(default_port));
console.log("Server available at port 5001...");
