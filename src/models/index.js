import Sequelize from 'sequelize';

const SQLdbName = 'COMPRATEC_DB';
const SQLUserName = 'admin_proyecto2';
const SQLPassName = 'asSEqW$%.sdRe0';
const hostLink = 'proyecto2sql.c7cr1tkqrdgz.us-east-1.rds.amazonaws.com';

const sequelize = new Sequelize (
  SQLdbName,
  SQLUserName,
  SQLPassName,
  {
    host: hostLink,  
    dialect: 'postgres',
    define: {
      timestamps: false
    }
  },
);

const db = {
  Order: sequelize.import('./order'),
};

db.sequelize = sequelize;

export default db;