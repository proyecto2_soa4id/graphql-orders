export default (sequelize, DataTypes) => {
  const Order = sequelize.define('order', {
    owner: DataTypes.STRING,
    type: DataTypes.STRING
  });

  return Order;
};